package connection;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicButtonUI;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.SwingConstants;
import javax.swing.JTable;
import javax.swing.JList;
import javax.swing.JTextArea;
import javax.swing.JTabbedPane;

public class GUI extends JFrame {

	private JPanel contentPane;
	private JTable users;
	private JTable rooms;
	private JTabbedPane tabbedPane;
	private JTextArea textArea;
	private List<JList> lists = new ArrayList<JList>();
	private Boolean me = false;
	private ObjectOutputStream out;
	private String username;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//GUI frame = new GUI();
					//frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI(ObjectOutputStream out) {
		this.out = out;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 600);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		//Create the heading
		JLabel lblChatty = new JLabel("Chatty");
		lblChatty.setHorizontalAlignment(SwingConstants.CENTER);
		lblChatty.setFont(new Font("Lucida Grande", Font.BOLD, 52));
		contentPane.add(lblChatty, BorderLayout.NORTH);
		
		//Create user list and their statues
		String[] columnHeadings = {"Users", "Status"};
		Object[][] data = {};
		DefaultTableModel model = new DefaultTableModel(data, columnHeadings) {
		
			
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
			
		};
		
		
		users = new JTable(model);
		users.setSize(200, 150);
		
		
//		JPanel listPanel = new JPanel();
//		listPanel.setSize(300,150);		
//		listPanel.setLayout(new GridLayout(2,1));
		JScrollPane usersScrollPane = new JScrollPane(users);
		Dimension d = users.getPreferredSize();
		usersScrollPane.setPreferredSize(new Dimension(d.width, d.height));
		//listPanel.add(usersScrollPane);
		contentPane.add(usersScrollPane, BorderLayout.EAST);
		
		//Create rooms list and their statues
		String[] columnHeadings1 = {"Rooms", "Status"};
		//Object[][] data1 = {{"Rugby", "Online"}, {"Kraaie", "Online"}};
//		DefaultTableModel model1 = new DefaultTableModel(data1, columnHeadings1) {
//			
//			@Override
//			public boolean isCellEditable(int row, int column) {
//				return false;
//			}
//		};
//		
//		rooms = new JTable(model1);
//		rooms.setSize(200, 150);
//
//		JScrollPane roomsScrollPane = new JScrollPane(rooms);
//		Dimension d1 = rooms.getPreferredSize();
//		roomsScrollPane.setPreferredSize(new Dimension(d1.width, d1.height));
//		listPanel.add(roomsScrollPane);
		
		//Create text area and send button
		JPanel messagePanel = new JPanel();
		messagePanel.setSize(400, 150);
		messagePanel.setLayout(new FlowLayout(FlowLayout.LEADING, 0 , 0));
		contentPane.add(messagePanel, BorderLayout.SOUTH);
		
		textArea = new JTextArea(3,41);
		textArea.setSize(700, 140);
		textArea.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode()==KeyEvent.VK_ENTER){
					if (textArea.getText().trim().length() > 0) {
						sendMessage();
					}
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		messagePanel.add(textArea);
		
		JButton send = new JButton("Send");
		send.setSize(100, 80);
		send.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (textArea.getText().trim().length() > 0) {
					sendMessage();
				}
			}
			
		});
		
		messagePanel.add(Box.createHorizontalStrut(40));
		messagePanel.add(send);
		
		//Create tabbed pane for chats
		JPanel chatPanel = new JPanel();
		chatPanel.setSize(600,600);
		contentPane.add(chatPanel, BorderLayout.CENTER);
		
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setPreferredSize(new Dimension(470,440));
		JComponent tab1 = makeTextPanel();
		tabbedPane.addTab("Room", null, tab1, "Does nothing");		
		
		tabbedPane.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				int index = tabbedPane.getSelectedIndex();
				tabbedPane.setBackgroundAt(index, Color.WHITE);
				
			}
			
		});
		
		chatPanel.add(tabbedPane);
		newChat();
	}
	
	
	/**
	 * Add User to list of users
	 * @param name	Name of user to add
	 * @param status	Initial status of user
	 */
	public void addUser(String name, String status) {
		
		DefaultTableModel model = (DefaultTableModel) users.getModel();
		Object[] row = {name, status};
		model.addRow(row);
		
	}
	
	/**
	 * Remove a user from the user list
	 * @param name	the name of the user to remove
	 */
	public void removeUser(String name) {
		System.out.println(name);
		int count = users.getRowCount();
		for (int i = 0 ; i < count ; i++) {
			
			if (users.getModel().getValueAt(i, 0) == name) {
				((DefaultTableModel) users.getModel()).removeRow(i);
				break;
			}
		}		
	}
	
	/**
	 * Creates new tab to start a chat with the selected user
	 */
	public void newChat() {
		
		users.addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent me) {
		        if (me.getClickCount() == 2) {
		        	
		        	int index = tabbedPane.getTabCount();
		        	Point pnt = me.getPoint();
	                int row = users.rowAtPoint(pnt);
	                String user = (String) users.getModel().getValueAt(row, 0);
	                
	                for (int i = 0 ; i < index ; i++) {
	                	
	                	String label = tabbedPane.getTitleAt(i);
	                	if (label == user) {
			
	                		tabbedPane.setSelectedIndex(i);
	                		return;
						}
	                	
	                }
	                
		            createTab(user);
		            tabbedPane.setSelectedIndex(index);
		            
		        }
		    }
		});
		
	}
	
	/**
	 * Add room to list of rooms
	 * @param name	Name of room
	 * @param status	Initial status of room
	 */
	public void addRoom(String name, String status) {
		
		DefaultTableModel model = (DefaultTableModel) rooms.getModel();
		Object[] row = {name, status};
		model.addRow(row);
		
	}
	
	/**
	 * Send message to current tab when 'Enter' is pressed or Send button
	 */
	public void sendMessage() {
		String title = tabbedPane.getTitleAt(tabbedPane.getSelectedIndex());
		int type;
		
		if (title.equals("Room")) {
			type = 0;
		} else {
			type = 1;
		}
		Message newmessage = new Message(type, username, title, textArea.getText());
		addMessage(username, textArea.getText(), title, type);
		textArea.setText("");
				
    	try {
			out.writeObject(newmessage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Add message to specific chatroom or user
	 * @param name	Name of user sending the message
	 * @param message	Message string the user is sending
	 * @param tab	Name of tab to select. Often opposite user name
	 * @param type	Determines function of message: 0 - chatroom, 1 - private, 2 - add user to list, 3 - display online, 4 - display offline
	 */
	public void addMessage(String name, String message, String tab, int type) {
		
		int count = tabbedPane.getTabCount();
		boolean tabExists = false;

		for (int i = 0 ; i < count ; i++) {
			String label = tabbedPane.getTitleAt(i);
			if (label.equals(tab)) {

				tabExists = true;
				DefaultListModel model = (DefaultListModel) lists.get(i).getModel();
				if (tabbedPane.getSelectedIndex() != i) {
					tabbedPane.setBackgroundAt(i, Color.GREEN);
				}
				
				if (type == 0 || type == 1) {
					model.addElement(name + ": " + message);
				} else if (type == 3){
					model.addElement(name + " is now online.");
				} else if (type == 4) {
					model.addElement(name + " is now offline.");
				} else if (type == 5) {
					model.addElement("Your previous message was not received. The user is offline.");
				}
			}
		}
		
		if (!tabExists) {
			int index = tabbedPane.getTabCount();
			createTab(tab);
			tabbedPane.setBackgroundAt(index, Color.GREEN);
			DefaultListModel model = (DefaultListModel) lists.get(index).getModel();
			model.addElement(name + ": " + message);
		}	
	}
	
	/**
	 * Create components on each tab
	 * @param text
	 * @return
	 */
	protected JComponent makeTextPanel() {
        JPanel panel = new JPanel(false);
        panel.setSize(600,600);
        
      //Create Jlist for message view
      	DefaultListModel chat = new DefaultListModel();
      	JList chats = new JList(chat);
      	ListCellRenderer renderer = new ChatListCellRenderer();
      	chats.setCellRenderer(renderer);
        lists.add(chats);
      	
        panel.setLayout(new GridLayout(1, 1));
        panel.add(chats);
        return panel;
    }
	
	/**
	 * Creates new tab
	 * @param title	Title of the tab
	 */
	public void createTab(String title) {
		
		JComponent tab = makeTextPanel();
		tabbedPane.addTab(title, null, tab, "Chat");
		int index = tabbedPane.getTabCount()-1;
		tabbedPane.setTabComponentAt(index, new ButtonTabComponent(tabbedPane, lists));
	}
	

	/**
	 * Sets the username of the client.
	 * @param username determined by client
	 */
	public void setUsername(String username) {
		this.username = username;
	}	
}

/**
 * Class that redesigns the JList to have a chat format
 */
class ChatListCellRenderer implements ListCellRenderer {
	
	 protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

	  public Component getListCellRendererComponent(JList list, Object value, int index,
	      boolean isSelected, boolean cellHasFocus) {
		  
		Border noFocusBorder = new EmptyBorder(5, 1, 1, 1);
		  
	    JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index,
	        isSelected, cellHasFocus);
	    
	    renderer.setBorder(noFocusBorder);
	    renderer.setForeground(Color.RED);
	    return renderer;
	  }
	
}



