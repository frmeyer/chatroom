package connection;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;
import javax.swing.JButton;

public class Login extends JFrame {

	private JPanel contentPane;
	private JLabel errMsg;
	private JTextField username;
	private String user;
	private boolean check;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		check = false;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 350, 180);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		
		//Set up of heading
		JLabel lblChatty = new JLabel("Chatty");
		lblChatty.setHorizontalAlignment(SwingConstants.CENTER);
		lblChatty.setFont(new Font("Lucida Grande", Font.PLAIN, 40));
		contentPane.add(lblChatty, BorderLayout.NORTH);
		
		//Set up of JButton and action on click
		JButton btnConnect = new JButton("Connect");
		btnConnect.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				//Add switch over here
				check = true;
			}
			
		});
		contentPane.add(btnConnect, BorderLayout.SOUTH);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		
		//Add JLabel to display error message
		errMsg = new JLabel("l");
		errMsg.setForeground(Color.LIGHT_GRAY);
		panel.add(errMsg);
		
		panel.add(Box.createRigidArea(new Dimension(0, 10)));
		username = new JTextField(30);
		
		//Add JTextfield to get username
		panel.add(username);
		panel.add(Box.createRigidArea(new Dimension(0, 10)));
		contentPane.add(panel, BorderLayout.CENTER);
	}
	
	/**
	 * Sets the text of the JLabel which displays an error message
	 * @param text	The error text to be displayed
	 */
	protected void setErrorMessage(String text) {
		
		errMsg.setForeground(Color.RED);
		errMsg.setText(text);		
	}
	
	/**
	 * Return the username typed in by the client.
	 */
	public String getUserText() {
		return username.getText();
	}
	
	/**
	 * Return the unique username.
	 */
	public String getUsername() {
		return user;
	}
	
	/**
	 * Set the unique username.
	 */
	public void setUsername(String username) {
		user = username;
	}
	
	/**
	 * Returns the check variable, which is true if a new username must be tested.
	 */
	public boolean checkNow() {
		return check;
	}
	
	/**
	 * Returns the check variable, which is true if a new username must be tested.
	 */
	public void stopCheck() {
		check = false;
	}

}
