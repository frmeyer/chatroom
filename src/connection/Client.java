package connection;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JOptionPane;

public class Client {

	public static void main(String[] args) throws ClassNotFoundException {
		
		String hostName = "localhost"; //args[0];
        int portNumber = 4444; //Integer.parseInt(args[1]);
        String username = null;
        
        Socket socket;
        ObjectOutputStream out;
    	ObjectInputStream in;
    	BufferedReader stdIn;
    	
        try {        	
            socket = new Socket(hostName, portNumber);
        	out = new ObjectOutputStream(socket.getOutputStream());
        	in  = new ObjectInputStream(socket.getInputStream());
            stdIn = new BufferedReader(new InputStreamReader(System.in));
            
            /* Get username from client and check if unique */
            Login login = new Login();            
			login.setVisible(true);
			
			Message userMessage;
			while (login.getUsername() == null) {
				//System.out.println();
				try {
					Thread.sleep(100);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (login.checkNow()) {
					
					userMessage = new Message(3, null, null, login.getUserText());
					out.writeObject(userMessage);
					userMessage = (Message)in.readObject();
					
					if (userMessage.getContent() != null) {
						username = userMessage.getContent();
						login.setUsername(userMessage.getContent());
					} else {
						login.setErrorMessage("Username already exists.");
						login.stopCheck();
					}
				}
			}
			login.dispose();
        
        	GUI frame = new GUI(out);
			frame.setVisible(true);
			frame.setUsername(username);
			frame.setTitle(username);
			
            String userInput;
            boolean listen = true;
            Message newmessage;
            while (listen) {
            	newmessage = (Message)in.readObject();
            	
            	if (newmessage.getType() == 0) { //Chatroom
            		frame.addMessage(newmessage.getSender(), newmessage.getContent(), "Room", 0);
            	} else if (newmessage.getType() == 1) { //Private message
            		frame.addMessage(newmessage.getSender(), newmessage.getContent(), newmessage.getSender(), 1);
            	} else if (newmessage.getType() == 2) { //New online user to list
            		frame.addUser(newmessage.getContent(), "Online");
            	} else if (newmessage.getType() == 3) { //Display that new user online
            		frame.addMessage(newmessage.getContent(), null, "Room", 3);
            	} else if (newmessage.getType() == 4) { //Remove user from list 
            		frame.removeUser(newmessage.getContent());
            		frame.addMessage(newmessage.getContent(), null, "Room", 4);
            	} else if (newmessage.getType() == 5) {
            		frame.addMessage(newmessage.getSender(), newmessage.getContent(), newmessage.getSender(), 5);
            	} else if (newmessage.getType() == 6) {
            		frame.dispose();
            	}
            }
            
			final Socket tempSocket = socket;
			
			frame.addWindowListener(new WindowAdapter() {
			    @Override
			    public void windowClosing(WindowEvent e) {
			    	try {
			    		//Message lastMessage = new Message(4, null, null, null);
						//out.writeObject(lastMessage));
						tempSocket.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			    }
			});
        } catch (EOFException eof) {
        	//eof.printStackTrace();
        	JOptionPane.showConfirmDialog(null, "The server has disconnected.", "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE);
        	System.exit(1);        	
        }
        catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
        	JOptionPane.showConfirmDialog(null, "The server is not connected.", "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE);
        	System.exit(1); 
        } 
    }
}
