package connection;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

public class ClientThread extends Thread{

	Socket clientSocket = null;
	String username = "";
	public static ArrayList<ClientThread> users = new ArrayList<ClientThread>();
	private boolean isRunning = true;
	
	ObjectOutputStream out;
	ObjectInputStream in;

    public ClientThread(Socket socket, ArrayList<ClientThread> users) {
        this.clientSocket = socket;
        this.users = users;  
    }
    
    public void run() {

        try {
        	out = new ObjectOutputStream(clientSocket.getOutputStream());
        	in  = new ObjectInputStream(clientSocket.getInputStream());
           
            Message recMessage;
            Message newMessage;
            
            /* Get the user name from the client and check if unique */
            while (username.equals("")) {
            	recMessage = (Message)in.readObject();
            	boolean isUnique = true;
            	
            	for (ClientThread client : users) {
            		if (client.getUsername().equals(recMessage.getContent())) {
            			newMessage = new Message(3, null, null, null);
            			out.writeObject(newMessage);
            			isUnique = false;
            			break;
            		}
            	}
            	if (isUnique) {
            		username = recMessage.getContent();
            		newMessage = new Message(3, null, null, username);
            		out.writeObject(newMessage);  	            		
            	}
            }
            
            users.add(this);
            System.out.println(username + " is now connected.");
              
            /* Add user list and add new user to other lists*/
            for (ClientThread client : users) {
            	
            	if (client.getUsername() != null) {
            		addNewUser(client.getUsername());
            	}
            	
            	if (!client.getUsername().equals(username)) {
            		client.addNewUser(username);
            	}
            }
            
            /* Display message that user is online */
            for (ClientThread client : users) {
        		if (!client.getUsername().equals(username)) {
        			newMessage = new Message(3, username, null, username);
        			client.writeToClient(newMessage);
        		}
        	}
            
            /* Wait to receive messages  and send message to destination*/
            while (isRunning) {
            	
            	recMessage = (Message)in.readObject();
            	
            	if (recMessage.getType() == 0) { // Chatroom
	            	for (ClientThread client : users) {
	            		if (!client.getUsername().equals(username)) {
	            			client.writeToClient(recMessage);
	            		}
	            	}       
            	} else if (recMessage.getType() == 1) { //Private
            		boolean userOffline = true;
            		for (ClientThread client : users) {
            			if (client.getUsername().equals(recMessage.getReceiver())) {
            				client.writeToClient(recMessage);
            				userOffline = false;
            			}            			
	            	}  
            		if (userOffline) {
            			for (ClientThread client : users) {
                			if (client.getUsername().equals(recMessage.getSender())) {
                				Message offlineMessage = new Message(5, recMessage.getReceiver(), recMessage.getSender(), recMessage.getReceiver());
                				client.writeToClient(offlineMessage);
                			}            			
    	            	}  
            		}
            	}
            }
            clientSocket.close();
        } catch(EOFException ex){
            try {            	
            	/* Remove user from array list */
            	int i = 0;
            	int index = 0;
            	for (ClientThread client : users) {
            		if (client.getUsername().equals(username)) {
            			index = i;
             		}
             		i++;
             	}  
            	users.remove(index);
            	System.out.println(username + " is now disconnected.");
            	
            	/* Display user offline to all users */
            	Message removeMessage;
            	for (ClientThread client : users) {
             		removeMessage = new Message(4, username, null, username);
             		client.writeToClient(removeMessage);
             	}          	
				clientSocket.close();
			} catch(SocketException se) {
	        	
	        } catch (IOException e) {
				// TODO Auto-generated catch block
	        	System.out.println("jissus");
				e.printStackTrace();
			} 
        } catch (SocketException se) {
        	
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }
    
    public void writeToClient(Message message) {
    	try {
			out.writeObject(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public void addNewUser(String user) {
    	Message message = new Message(2, null, null, user);
    	try {
			out.writeObject(message);
		} catch (SocketException se) {
			
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public String getUsername() {
    	return username;
    }
    
    public void disconnect() {
    	try {
    		isRunning = false;
    		in.close();
			clientSocket.close();			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}
