package connection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

public class Server extends Thread {
	
	public static ArrayList<ClientThread> users = new ArrayList<ClientThread>();
	public static boolean listen = true;
	private ServerSocket serverSocket;
	
	public Server() throws ClassNotFoundException {
		        
       
	}
	
	public void run() {
		 int portNumber = 4444; 
	        
	        try {
	            serverSocket = new ServerSocket(portNumber);
	         
		        while (listen) {
		        	new ClientThread(serverSocket.accept(), users).start();
			    } 
		        System.out.println("awe ma se naai");
	            serverSocket.close();
	            
	        } catch (SocketException se) {
	        	
	        } catch (Exception e) {
	            System.out.println("Exception caught when trying to listen on port "
	                + portNumber + " or listening for a connection");
	            System.out.println(e.getMessage());
	            e.printStackTrace();
	        }
	}
	
	public void setListen(boolean val) {
		listen = val;
		System.out.println("awe");
	}
	
	public void disconnect() {
		System.out.println("awe");
		
		Message terminateMessage = new Message(6, null, null, null);
		int i = 0;
		for (ClientThread client : users) {
			client.writeToClient(terminateMessage);
			client.disconnect();
		}
		users.removeAll(users);
		
		try {
			serverSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
