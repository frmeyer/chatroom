package connection;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;
import javax.swing.JButton;

public class ServerGUI extends JFrame {

	private JPanel contentPane;
	private Server server;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ServerGUI frame = new ServerGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ServerGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 120);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout());
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		
		JLabel lblChattyServer = new JLabel("Chatty Server");
		lblChattyServer.setHorizontalAlignment(SwingConstants.CENTER);
		lblChattyServer.setFont(new Font("Lucida Grande", Font.PLAIN, 40));
		contentPane.add(lblChattyServer, BorderLayout.NORTH);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		
		final JButton start = new JButton("Start");
		final JButton stop = new JButton("Stop");	
		stop.setEnabled(false);
		
		start.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Starting server here			
				start.setEnabled(false);
				stop.setEnabled(true);
				try {
					server = new Server();
					server.start();
					System.out.println("Server is now running.");	
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		});		
		
		
		stop.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Stoping sever here
				//server.setListen(false);
				server.disconnect();
				start.setEnabled(true);
				stop.setEnabled(false);
				System.out.println("Server has stopped running.");	
			}
			
		});
		
		buttonPanel.add(start);
		buttonPanel.add(Box.createHorizontalStrut(40));
		buttonPanel.add(stop);
		contentPane.add(buttonPanel);
	}
}