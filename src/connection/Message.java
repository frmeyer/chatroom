package connection;
import java.io.Serializable;

public class Message implements Serializable {

	/**
	 * Private variables containing the message information.
	 */
	private int type;
	private String sender;
	private String receiver;
	private String content;
	
	/**
	 * Default constructor that populates private variables.
	 * @param type Type of message
	 * @param sender Username of sender
	 * @param receiver Username of receiver
	 * @param content Content of message
	 */
	public Message(int type, String sender, String receiver, String content) {
		this.type = type;
		this.sender = sender;
		this.receiver = receiver;
		this.content = content;
	}
	
	/**
	 * @return Type of message (chatroom is 0, private is 1, new user online is 2).
	 */
	public int getType() {
		return type;
	}
	
	/**
	 * @return Username of message sender.
	 */
	public String getSender() {
		return sender;
	}
	
	/**
	 * @return Username of message receiver (if private message).
	 */
	public String getReceiver() {
		return receiver;
	}
	
	/**
	 * @return Content of the message.
	 */
	public String getContent() {
		return content;
	}

}
